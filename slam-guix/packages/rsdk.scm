;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Slambert <labzero@protonmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (slam-guix packages rsdk)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xml)
  #:use-module (slam-guix packages stb)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (srfi srfi-1)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module ((nonguix licenses) :prefix non-license:))

(define-public rsdk3
  (package
   (name "rsdk3")
   (version "1.3.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   (url "https://github.com/Rubberduckycooly/Sonic-CD-11-Decompilation")
	   (recursive? #t)
	   (commit (string-append version))))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0blipajk9is1nf1n8qxyq1529m6y63v7dxk5g559d6yiw7y0abvz"))
     (modules '((guix build utils)))))
   (build-system gnu-build-system)
   (arguments
    `(#:tests? #f
      #:phases
      (modify-phases %standard-phases
		     (delete 'configure)
		     (replace 'install
			      (lambda* (#:key inputs outputs #:allow-other-keys)
				(mkdir-p (string-append (assoc-ref %outputs "out") "/bin"))
				(copy-file (string-append "RSDKv3")
					   (string-append (assoc-ref %outputs "out") "/bin/RSDKv3")))))))
   (native-inputs
    (list pkg-config
	  python-3))
   (inputs
    (list libtheora
	  glew
	  sdl2
	  libvorbis
	  libogg
	  alsa-lib
	  pulseaudio))
   (home-page "https://github.com/Rubberduckycooly/Sonic-CD-11-Decompilation")
   (synopsis "A Full Decompilation of Sonic CD (2011) & Retro Engine (v3)")
   (description
    "rsdk3 is a full decompilation of Sonic CD (2011) & Retro Engine (v3).")
   (license (list (non-license:nonfree "file://LICENSE.md")))))

(define-public rsdk5u
  (let ((commit "2131e945dbbc8dc6567c4202a41941face3d8f0b")
        (revision "1"))
    (package
     (name "rsdk5u")
     (version (git-version "1.0.0" revision commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/Rubberduckycooly/RSDKv5-Decompilation")
	     (recursive? #t)
             (commit commit)))
       (file-name (git-file-name name version))
       (sha256
	(base32
	 "1404m7bjfrgm3lvzi8g131papdnkpf7a8filph75s609zylhjxs1"))
       (modules '((guix build utils)))))
     (build-system gnu-build-system)
     (arguments
      `(#:tests? #f
	#:phases
	(modify-phases %standard-phases
		       (delete 'configure)
		       (add-after 'unpack 'fix-portaudio
				  (lambda* (#:key inputs outputs #:allow-other-keys)
				    (substitute* "makefiles/Linux.cfg"
						 (("portaudio")
						  (string-append "portaudio-2.0")))))
		       (replace 'install
				(lambda* (#:key inputs outputs #:allow-other-keys)
				  (mkdir-p (string-append (assoc-ref %outputs "out") "/bin"))
				  (copy-file (string-append "bin/Linux/OGL/RSDKv5U")
					     (string-append (assoc-ref %outputs "out") "/bin/RSDKv5U")))))
	#:make-flags
	(list (string-append "RSDK_ONLY=1")
	      (string-append "CC=gcc"))))
     (native-inputs
      (list pkg-config
	    python-3))
     (inputs
      (list libtheora
	    glew
	    zlib
	    sdl2
	    stb-vorbis
	    portaudio
	    alsa-lib
	    pulseaudio
	    glfw))
     (home-page "https://github.com/Rubberduckycooly/RSDKv5-Decompilation/")
     (synopsis "A complete decompilation of Retro Engine v5 and v5Ultimate.")
     (description
      "A complete decompilation of Retro Engine v5 and v5Ultimate.")
     (license (list (non-license:nonfree "file://LICENSE.md"))))))
