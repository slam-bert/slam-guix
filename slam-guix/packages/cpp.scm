;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Slambert <labzero@protonmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (slam-guix packages cpp)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system meson)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages tls)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) :prefix license:))
      
(define-public tomlplusplus
  (package
   (name "tomlplusplus")
   (version "3.3.0")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/marzer/tomlplusplus")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
	    (sha256
             (base32 "0lvv4dq2fhadvb9hbf89nvg5r2pfjlccc4nawl0kx6rfw56grm90"))))
   (build-system meson-build-system)
   (native-inputs
    (list cmake))
   (home-page "https://marzer.github.io/tomlplusplus/")
   (synopsis "Header-only TOML config file parser and serializer for C++17")
   (description "tomlplusplus is a header-only TOML config file parser and serializer for C++17.")
   (license license:expat)))

(define-public xbyak
  (package
   (name "xbyak")
   (version "6.72")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/herumi/xbyak")
		  (commit (string-append "v" version))))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32 "1ks4bkvph70z2xmw9pmc8x60fa9y4bxdmyacjb403zfrpayywk12"))))
   (build-system meson-build-system)
   (native-inputs
    (list cmake))
   (home-page "https://github.com/herumi/xbyak/")
   (synopsis "A C++ JIT assembler for x86 (IA32), x64 (AMD64, x86-64)")
   (description "Xbyak is a C++ header library that enables dynamically to assemble x86(IA32), x64(AMD64, x86-64) mnemonic.")
   (license license:bsd-3)))
