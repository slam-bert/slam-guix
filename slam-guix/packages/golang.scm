;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Slambert <labzero@protonmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (slam-guix packages golang)
  #:use-module (guix build-system go)
  #:use-module (guix packages)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) :prefix license:))

(define-public go-github-com-fhs-gompd
  (package
    (name "go-github-com-fhs-gompd")
    (version "2.3.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/fhs/gompd")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32 "0kq7mkqw32z7b55agbvazkfl16wq947gwbdk05a0kmzfy83gn5i4"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "github.com/fhs/gompd/v2"
       #:phases
       (modify-phases %standard-phases
                      (replace 'build
                               (lambda arguments
                                 (for-each
                                  (lambda (directory)
                                    (apply (assoc-ref %standard-phases 'build)
                                           `(,@arguments #:import-path ,directory)))
                                  (list
                                   "github.com/fhs/gompd/v2/mpd"))))
                      (replace 'check
                               (lambda arguments
                                 (for-each
                                  (lambda (directory)
                                    (apply (assoc-ref %standard-phases 'check)
                                           `(,@arguments #:import-path ,directory)))
                                  (list
                                   "github.com/fhs/gompd/v2/mpd"))))
                      (replace 'install
                               (lambda arguments
                                 (for-each
                                  (lambda (directory)
                                    (apply (assoc-ref %standard-phases 'install)
                                           `(,@arguments #:import-path ,directory)))
                                  (list
                                   "github.com/fhs/gompd/v2/mpd")))))))
    (home-page "https://github.com/fhs/gompd")
    (synopsis "A Go package for accessing Music Player Daemon (MPD)")
    (description "Client side library for MPD (Music Player Daemon) for
Go Programming Language.")
    (license license:expat)))

(define-public go-github-com-gotk3-gotk3
  (package
    (name "go-github-com-gotk3-gotk3")
    (version "0.6.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gotk3/gotk3")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32 "0zphsrbm0vi3v70xr9kzds5d6svhwcz2kfv5swrik74nic509ybz"))))
    (build-system go-build-system)
    (arguments
     `(#:tests? #f
       #:import-path "github.com/gotk3/gotk3"
       #:phases
       (modify-phases %standard-phases
                      (replace 'build
                               (lambda arguments
                                 (for-each
                                  (lambda (directory)
                                    (apply (assoc-ref %standard-phases 'build)
                                           `(,@arguments #:import-path ,directory)))
                                  (list
                                   "github.com/gotk3/gotk3/cairo"
                                   "github.com/gotk3/gotk3/gdk"
                                   "github.com/gotk3/gotk3/gio"
                                   "github.com/gotk3/gotk3/glib"
                                   "github.com/gotk3/gotk3/gtk"
                                   "github.com/gotk3/gotk3/internal/callback"
                                   "github.com/gotk3/gotk3/internal/closure"
                                   "github.com/gotk3/gotk3/internal/slab"
                                   "github.com/gotk3/gotk3/pango"))))
                      (replace 'check
                               (lambda arguments
                                 (for-each
                                  (lambda (directory)
                                    (apply (assoc-ref %standard-phases 'check)
                                           `(,@arguments #:import-path ,directory)))
                                  (list
                                   "github.com/gotk3/gotk3/cairo"
                                   "github.com/gotk3/gotk3/gdk"
                                   "github.com/gotk3/gotk3/gio"
                                   "github.com/gotk3/gotk3/glib"
                                   "github.com/gotk3/gotk3/gtk"
                                   "github.com/gotk3/gotk3/internal/callback"
                                   "github.com/gotk3/gotk3/internal/closure"
                                   "github.com/gotk3/gotk3/internal/slab"
                                   "github.com/gotk3/gotk3/pango"))))
                      (replace 'install
                               (lambda arguments
                                 (for-each
                                  (lambda (directory)
                                    (apply (assoc-ref %standard-phases 'install)
                                           `(,@arguments #:import-path ,directory)))
                                  (list
                                   "github.com/gotk3/gotk3/cairo"
                                   "github.com/gotk3/gotk3/gdk"
                                   "github.com/gotk3/gotk3/gio"
                                   "github.com/gotk3/gotk3/glib"
                                   "github.com/gotk3/gotk3/gtk"
                                   "github.com/gotk3/gotk3/internal/callback"
                                   "github.com/gotk3/gotk3/internal/closure"
                                   "github.com/gotk3/gotk3/internal/slab"
                                   "github.com/gotk3/gotk3/pango")))))))
    (native-inputs (list pkg-config))
    (inputs (list gdk-pixbuf
                  glib
                  gtk+))
    (home-page "https://github.com/gotk3/gotk3")
    (synopsis "Go bindings for GTK3")
    (description "The gotk3 project provides Go bindings for GTK 3 and
dependent projects.")
    (license license:isc)))
