;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Slambert <labzero@protonmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (slam-guix packages wayland)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system meson)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module ((srfi srfi-1) #:hide (zip))
  #:use-module ((guix licenses) :prefix license:)
  #:use-module ((nonguix licenses) :prefix non-license:))

(define-public libdecor
  (package
    (name "libdecor")
    (version "0.1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.freedesktop.org/libdecor/libdecor")
                    (recursive? #t)
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "015x6nf6hndc4m16p42ki956vg936ajh27h0cghkc34jlc5amgpi"))))
    (build-system meson-build-system)
    (native-inputs
     (list pkg-config
           cmake))
    (inputs
     (list wayland
	   wayland-protocols
	   dbus
	   cairo
	   pango
	   mesa
	   libglvnd
	   egl-wayland
	   libxkbcommon))
    (home-page "https://gitlab.freedesktop.org/libdecor/libdecor")
    (synopsis "A client-side decorations library for Wayland clients")
    (description "libdecor is a library that can help Wayland clients draw
window decorations for them. It aims to provide multiple backends that
implements the decoration drawing.")
    (license license:expat)))

(define-public glfw-wayland
  (package
    (name "glfw-wayland")
    (version "3.3.8")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/glfw/glfw"
                                  "/releases/download/" version
                                  "/glfw-" version ".zip"))
              (sha256
               (base32
                "19hyj7cnj9i0pvsbr9b5wlikh687b17vmfdr3bwx2gaarj1m00jd"))))
    (build-system cmake-build-system)
    (arguments
     '(#:tests? #f ; no test target
       #:configure-flags '("-DBUILD_SHARED_LIBS=ON"
			   "-DGLFW_USE_WAYLAND=ON"
			   "-DGLFW_USE_LIBDECOR=ON")))
    (native-inputs
     (list doxygen
	   extra-cmake-modules
	   pkg-config
	   unzip
	   wayland-protocols))
    (inputs
     (list libdecor
           libxkbcommon
	   wayland))
    (propagated-inputs
     (list mesa
           libx11
           libxrandr
           libxi
           libxinerama
           libxcursor
           libxxf86vm))
    (home-page "https://www.glfw.org")
    (synopsis "OpenGL application development library")
    (description
     "GLFW is a library for OpenGL, OpenGL ES and Vulkan development for
desktop computers.  It provides a simple API for creating windows, contexts
and surfaces, receiving input and events.")
    (license license:zlib)))
