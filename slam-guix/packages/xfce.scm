;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Slambert <labzero@protonmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (slam-guix packages xfce)
  #:use-module (guix build-system copy)
  #:use-module (gnu packages base)
  #:use-module (gnu packages gtk)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) :prefix license:))

(define-public chicago95-theme
  (package
   (name "chicago95-theme")
   (version "3.0.1")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/grassmunk/Chicago95")
		  (commit (string-append "v" version))))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
	      "045qdbsqpghbfjr1nxvvpwsqa14kfsh9q5mlipnf8mbnrchh6xqh"))))
   (build-system copy-build-system)
   (arguments
    `(#:install-plan
      `(("Theme/Chicago95" "/share/themes/"))))
   (propagated-inputs
    (list gdk-pixbuf))
   (home-page "https://github.com/grassmunk/Chicago95")
   (synopsis "XFCE Windows 95 Total Conversion")
   (description "A rendition of everyone's favorite 1995 Microsoft operating
system for Linux.")
   (license license:gpl3+)))

(define-public chicago95-icons
  (package
   (name "chicago95-icons")
   (version "3.0.1")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/grassmunk/Chicago95")
		  (commit (string-append "v" version))))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
	      "045qdbsqpghbfjr1nxvvpwsqa14kfsh9q5mlipnf8mbnrchh6xqh"))))
   (build-system copy-build-system)
   (arguments
    `(#:install-plan
      `(("Icons/Chicago95" "share/icons/")
	("Icons/Chicago95-tux" "share/icons/"))))
   (propagated-inputs
    (list gdk-pixbuf))
   (home-page "https://github.com/grassmunk/Chicago95")
   (synopsis "XFCE Windows 95 Total Conversion")
   (description "A rendition of everyone's favorite 1995 Microsoft operating
system for Linux.")
   (license license:gpl3+)))

chicago95-icons
