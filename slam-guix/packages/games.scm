;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Slambert <labzero@protonmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (slam-guix packages games)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (guix packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages games)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages image)
  #:use-module (gnu packages javascript)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xorg)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) :prefix license:))

;; Largely taken from the quakespasm definition, as it's a fork.
(define-public ironwail
  (package
   (name "ironwail")
   (version "0.7.0")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/andrei-drexler/ironwail")
		  (commit (string-append "v" version))))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32 "1xsfn3jp2imgqabci7hh2y7aw4yiv4w3lgkddaiqbkryqmlx18pg"))))
   (build-system gnu-build-system)
   (arguments
    `(#:tests? #f
      #:make-flags
      '("CC=gcc"
        "USE_CODEC_FLAC=1"
        "USE_CODEC_MIKMOD=1"
        "USE_SDL2=1"
        "-CQuake")
      #:phases
      (modify-phases %standard-phases
		     (delete 'configure)
		     (add-after 'unpack 'fix-usr-paths
				(lambda* (#:key outputs #:allow-other-keys)
				  (let ((out (assoc-ref outputs "out")))
				    (substitute* "Quake/Makefile" (("/usr")
								   (string-append out))))))
		     (replace 'install
			      (lambda* (#:key outputs #:allow-other-keys)
				(let ((out (assoc-ref outputs "out")))
				  (mkdir-p (string-append out "/bin"))
				  (copy-file (string-append "Quake/ironwail")
					     (string-append out "/bin/ironwail"))))))))
   (native-inputs (list pkg-config))
   (inputs (list libmikmod
                 libvorbis
                 flac
                 mesa
                 libmad
                 sdl2
		 curl))
   (home-page "https://github.com/andrei-drexler/ironwail")
   (synopsis "High-performance QuakeSpasm fork")
   (description "A fork of the popular GLQuake descendant QuakeSpasm with
a focus on high performance instead of maximum compatibility, with a few
extra features sprinkled on top.")
   (license license:gpl2)))

(define openrct2-title-sequences
  (package
   (name "openrct2-title-sequences")
   (version "0.4.6")
   (source (origin
     (method url-fetch)
     (uri (string-append "https://github.com/OpenRCT2/title-sequences/releases/download/v"
                         version "/title-sequences.zip"))
     (file-name (string-append name "-" version ".zip"))
     (sha256
      (base32
       "160df56cwb4vl1h25k8rz2wqmpyfncd8zpmfdinzny0zmz6qk894"))))
   (build-system trivial-build-system)
   (native-inputs
    `(("bash" ,bash)
      ("coreutils" ,coreutils)
      ("unzip" ,unzip)))
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((out (assoc-ref %outputs "out"))
               (openrct2-title-sequences (string-append out
                                                        "/share/openrct2/title-sequences"))
               (source (assoc-ref %build-inputs "source"))
               (unzip (search-input-file %build-inputs "/bin/unzip")))
          (copy-file source (string-append ,name "-" ,version ".zip"))
          (invoke unzip (string-append ,name "-" ,version ".zip"))
          (delete-file (string-append ,name "-" ,version ".zip"))
          (mkdir-p openrct2-title-sequences)
          (copy-recursively "."
                            openrct2-title-sequences)
          #t))))
   (home-page "https://github.com/OpenRCT2/OpenRCT2")
   (synopsis "Title sequences for OpenRCT2")
   (description
    "openrct2-title-sequences is a set of title sequences for OpenRCT2.")
   (license license:gpl3+)))

(define openrct2-objects
  (package
   (name "openrct2-objects")
   (version "1.3.11")
   (source (origin
     (method url-fetch)
     (uri (string-append "https://github.com/OpenRCT2/objects/releases/download/v"
                         version "/objects.zip"))
     (file-name (string-append name "-" version ".zip"))
     (sha256
      (base32 "1d402z4jkf1jvixhayiwkd2w4zblk043jrsmj7js475i9y7di1dz"))))
   (build-system trivial-build-system)
   (native-inputs
    `(("bash" ,bash)
      ("coreutils" ,coreutils)
      ("unzip" ,unzip)))
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((out (assoc-ref %outputs "out"))
               (openrct2-objects (string-append out
                                                "/share/openrct2/objects"))
               (source (assoc-ref %build-inputs "source"))
               (unzip (search-input-file %build-inputs "/bin/unzip")))
          (copy-file source (string-append ,name "-" ,version ".zip"))
          (invoke unzip (string-append ,name "-" ,version ".zip"))
          (delete-file (string-append ,name "-" ,version ".zip"))
          (mkdir-p openrct2-objects)
          (copy-recursively "."
                            openrct2-objects)
          #t))))
   (home-page "https://github.com/OpenRCT2/OpenRCT2")
   (synopsis "Objects for OpenRCT2")
   (description
    "openrct2-objects is a set of objects for OpenRCT2.")
   (license license:gpl3+)))

(define openrct2-opensound
  (package
   (name "openrct2-opensound")
   (version "1.0.3")
   (source (origin
     (method url-fetch)
     (uri (string-append "https://github.com/OpenRCT2/OpenSoundEffects/releases/download/v"
                         version "/opensound.zip"))
     (file-name (string-append name "-" version ".zip"))
     (sha256
      (base32 "0v5fwi953xq10kvyba46r193rpdm8pacvq52660as96cz1pipgkh"))))
   (build-system trivial-build-system)
   (native-inputs
    `(("bash" ,bash)
      ("coreutils" ,coreutils)
      ("unzip" ,unzip)))
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((out (assoc-ref %outputs "out"))
               (openrct2-opensound (string-append out
                                                  "/share/openrct2/opensoundeffects"))
               (source (assoc-ref %build-inputs "source"))
               (unzip (search-input-file %build-inputs "/bin/unzip")))
          (copy-file source (string-append ,name "-" ,version ".zip"))
          (invoke unzip (string-append ,name "-" ,version ".zip"))
          (delete-file (string-append ,name "-" ,version ".zip"))
          (mkdir-p openrct2-opensound)
          (copy-recursively "."
                            openrct2-opensound)
          #t))))
   (home-page "https://github.com/OpenRCT2/OpenRCT2")
   (synopsis "Replacement sound effects for OpenRCT2")
   (description
    "openrct2-opensound is a replacement sound effects file for OpenRCT2.")
   (license (list
             license:expat
             license:cc-by-sa4.0))))

(define openrct2-openmusic
  (package
   (name "openrct2-openmusic")
   (version "1.3.1")
   (source (origin
     (method url-fetch)
     (uri (string-append "https://github.com/OpenRCT2/OpenMusic/releases/download/v"
                         version "/openmusic.zip"))
     (file-name (string-append name "-" version ".zip"))
     (sha256
      (base32 "018hmd15y7fdqx4yfilzxj620i5awsdwir3bwi8qxvvr0big1a8a"))))
   (build-system trivial-build-system)
   (native-inputs
    `(("bash" ,bash)
      ("coreutils" ,coreutils)
      ("unzip" ,unzip)))
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((out (assoc-ref %outputs "out"))
               (openrct2-openmusic (string-append out
                                                  "/share/openrct2/openmusic"))
               (source (assoc-ref %build-inputs "source"))
               (unzip (search-input-file %build-inputs "/bin/unzip")))
          (copy-file source (string-append ,name "-" ,version ".zip"))
          (invoke unzip (string-append ,name "-" ,version ".zip"))
          (delete-file (string-append ,name "-" ,version ".zip"))
          (mkdir-p openrct2-openmusic)
          (copy-recursively "."
                            openrct2-openmusic)
          #t))))
   (home-page "https://github.com/OpenRCT2/OpenRCT2")
   (synopsis "Alternative music for OpenRCT2")
   (description
    "openrct2-openmusic is an alternative, high quality soundtrack for OpenRCT2.")
   (license license:cc-by-sa4.0)))

(define-public openrct2-updated
  (package
   (name "openrct2")
   (version "0.4.6")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/OpenRCT2/OpenRCT2")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            (sha256
             (base32 "13kz5rkldcc5x2532xdf71c6c3rxn0jw72g6m5hq9a4q4isdh2hx"))))
   (build-system cmake-build-system)
   (arguments
    `(#:configure-flags (list "-DDOWNLOAD_OBJECTS=OFF"
                              "-DDOWNLOAD_TITLE_SEQUENCES=OFF"
                              "-DDOWNLOAD_OPENSFX=OFF"
                              "-DDOWNLOAD_OPENMSX=OFF")
      #:tests? #f                      ; tests require network access
      #:phases
      (modify-phases %standard-phases
          (add-after 'unpack 'fix-usr-share-paths&add-data
              (lambda* (#:key inputs outputs #:allow-other-keys)
                  (let ((titles (assoc-ref inputs "openrct2-title-sequences"))
                        (objects (assoc-ref inputs "openrct2-objects"))
                        (sounds (assoc-ref inputs "openrct2-opensound"))
                        (music (assoc-ref inputs "openrct2-openmusic")))
                    ;; Fix some references to /usr/share.
                    (substitute* "src/openrct2/platform/Platform.Linux.cpp"
                                 (("/usr/share")
                                  (string-append (assoc-ref %outputs "out") "/share")))
                    (copy-recursively
                     (string-append titles "/share/openrct2/title-sequences")
                     "data/sequence")
                    (copy-recursively
                     (string-append objects "/share/openrct2/objects")
                     "data/object")
                    (copy-recursively
                     (string-append sounds "/share/openrct2/opensoundeffects/assetpack")
                     "data/assetpack")
                    (copy-recursively
                     (string-append sounds "/share/openrct2/opensoundeffects/object")
                     "data/object")
                    (copy-recursively
                     (string-append music "/share/openrct2/openmusic/assetpack")
                     "data/assetpack")
                    (copy-recursively
                     (string-append music "/share/openrct2/openmusic/object")
                                     "data/object"))))
      (add-before 'configure 'get-rid-of-errors
                  (lambda _
                    ;; Don't treat warnings as errors.
                    (substitute* "CMakeLists.txt"
                                 (("-Werror") ""))
                    #t)))))
   (inputs `(("curl" ,curl)
             ("duktape" ,duktape)
             ("flac" , flac)
             ("fontconfig-minimal" ,fontconfig)
             ("freetype" ,freetype)
             ("icu4c" ,icu4c)
             ("jansson" ,jansson)
             ("nlohmann-json" ,nlohmann-json)
             ("libpng" ,libpng)
             ("libvorbis" ,libvorbis)
             ("libzip" ,libzip)
             ("mesa" ,mesa)
             ("openrct2-objects" ,openrct2-objects)
             ("openrct2-openmusic" ,openrct2-openmusic)
             ("openrct2-opensound" ,openrct2-opensound)
             ("openrct2-title-sequences" ,openrct2-title-sequences)
             ("openssl" ,openssl)
             ("sdl2" ,sdl2)
             ("speexdsp" ,speexdsp)
             ("zlib" ,zlib)))
   (native-inputs
    (list pkg-config))
   (home-page "https://github.com/OpenRCT2/OpenRCT2")
   (synopsis "Free software re-implementation of RollerCoaster Tycoon 2")
   (description "OpenRCT2 is a free software re-implementation of
RollerCoaster Tycoon 2 (RCT2).  The gameplay revolves around building and
maintaining an amusement park containing attractions, shops and facilities.

Note that this package does @emph{not} provide the game assets (sounds,
images, etc.)")
   ;; See <https://github.com/OpenRCT2/OpenRCT2/wiki/Required-RCT2-files>
   ;; regarding assets.
   (license license:gpl3+)))

;; Largely taken from the ioquake3 definition, as it's a fork.
(define-public quake3e
  (let ((revision "1")
        (commit "e445088d5342373537cdc9377a6be09916a57e67"))
    (package
     (inherit ioquake3)
     (name "quake3e")
     (version (git-version "2023-09-02" revision commit))
     (source (origin
              (method git-fetch)
              (uri (git-reference
		    (url "https://github.com/ec-/Quake3e")
		    (commit commit)))
              (file-name (git-file-name name version))
              (sha256
               (base32 "0515ngmfw8r85s2kbd9f5739wavww5q1zbjrk51j0ppjv3lzg3w5"))))
     (home-page "https://ioquake3.org/")
     (synopsis "Improved Quake III Arena engine")
     (description "This is a modern Quake III Arena engine aimed to be
fast, secure and compatible with all existing Q3A mods. It is based on
last non-SDL source dump of ioquake3 with latest upstream fixes applied."))))
