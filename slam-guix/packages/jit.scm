;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Slambert <labzero@protonmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (slam-guix packages jit)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages python)
  #:use-module (guix build-system meson)
  #:use-module (gnu packages cmake)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages check)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages tls)
  #:use-module (slam-guix packages c)
  #:use-module (slam-guix packages cpp)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) :prefix license:))

;; Only used by dynarmic.
(define merryhimecl
  (package
   (name "mcl")
   (version "0.1.12")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/merryhime/mcl")
		  (commit version)))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
	      "16nn3b0h1ngmdikhxvrd4bc5mriw4bs3dm7aikp97416crrlnvp0"))))
   (build-system cmake-build-system)
   (arguments
    `(#:tests? #f))
   (inputs
    (list catch2-3.3
	  fmt))
   (home-page "https://github.com/merryhime/mcl")
   (synopsis "merry's common library")
   (description
    "A collection of C++20 utilities which is common to a number of merry's
projects.")
   (license license:expat)))

(define-public dynarmic
  (package
   (name "dynarmic")
   (version "6.5.0")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/merryhime/dynarmic")
		  (commit version)))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
	      "0nl20kg787kbmkd51hy08gmk81qmw6ng1gqphccc9i80x1w7356x"))))
   (build-system cmake-build-system)
   (inputs
    (list boost
	  fmt
	  merryhimecl
	  robin-map
	  xbyak
	  zydis))
   (home-page "https://github.com/merryhime/dynarmic")
   (synopsis "An ARM dynamic recompiler")
   (description "A dynamic recompiler for ARMv6K, ARMv7A, and ARMv8.")
   (license license:bsd-0)))

dynarmic
