;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Jelle Licht <jlicht@fsfe.org>
;;; Copyright © 2019 Alex Griffin <a@ajgrf.com>
;;; Copyright © 2021 Pierre Langlois <pierre.langlois@gmx.com>
;;; Copyright © 2023 Slambert <labzero@protonmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (slam-guix packages minecraft)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages markup)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages java)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (slam-guix packages cpp)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module ((guix licenses) :prefix license:)
  #:use-module ((nonguix licenses) :prefix non-license:))
     
(define-public prismlauncher
  (package
   (name "prismlauncher")
   (version "7.2")
   (source (origin
	    (method url-fetch)
	    (uri (string-append "https://github.com/PrismLauncher/PrismLauncher/releases/download/"
				version "/PrismLauncher-" version ".tar.gz"))
	    (sha256
	     (base32
	      "1d0l8nl34vb4iff92ld28fvnsvwflgry4zgvlq9nha1j8mfbacsp"))))
   (build-system cmake-build-system)
   (arguments
    `(#:phases
      (modify-phases %standard-phases
		     (add-after 'install 'patch-paths
				(lambda* (#:key inputs outputs #:allow-other-keys)
				  (let* ((out            (assoc-ref outputs "out"))
					 (bin            (string-append out "/bin/prismlauncher"))
					 (xrandr         (assoc-ref inputs "xrandr"))
					 (qtwayland    (assoc-ref inputs "qtwayland")))
				    (wrap-program bin
						  `("PATH" ":" prefix (,(string-append xrandr "/bin")))
						  `("QT_PLUGIN_PATH" ":" prefix (,(string-append
										   qtwayland "/lib/qt5/plugins")))
						  `("LD_LIBRARY_PATH" ":" prefix
						    (,@(map (lambda (dep)
							      (string-append (assoc-ref inputs dep)
									     "/lib"))
							    '("libx11" "libxext" "libxcursor"
							      "libxrandr" "libxxf86vm" "pulseaudio" "mesa")))))
				    #t))))))
   (native-inputs (list ninja extra-cmake-modules))
   (inputs (list bash-minimal ;; for wrap-program
                 zlib
                 qtbase
		 qtsvg
		 qtwayland
		 qt5compat
		 cmark
		 gulrak-filesystem
		 tomlplusplus
                 xrandr
		 hicolor-icon-theme
                 libx11
                 libxext
                 libxcursor
                 libxrandr
                 libxxf86vm
                 pulseaudio
                 mesa))
   (propagated-inputs (list `(,openjdk17 "jdk")))
   (home-page "https://prismlauncher.org/")
   (synopsis "Free, open source launcher for Minecraft")
   (description
    "Allows you to have multiple, separate instances of Minecraft (each with
their own mods, texture packs, saves, etc), and helps you manage them and
their associated options with a simple interface.")
   (license (list license:gpl3          ;; PolyMC, launcher
                  license:expat         ;; MinGW runtime, lionshead, tomlc99
                  license:lgpl3         ;; Qt 5/6
                  license:lgpl3+        ;; libnbt++
                  license:lgpl2.1+      ;; rainbow (KGuiAddons)
                  license:isc           ;; Hoedown
                  license:silofl1.1     ;; Material Design Icons
                  license:lgpl2.1       ;; Quazip
                  license:public-domain ;; xz-minidec, murmur2, xz-embedded
                  license:bsd-3         ;; ColumnResizer, O2 (Katabasis fork),
                                        ;; gamemode, localpeer
                  license:asl2.0        ;; classparser, systeminfo
                  ;; Batch icon set:
                  (non-license:nonfree "file://COPYING.md")))))

(define-public papermc-1.20.1
    (package
      (name "papermc")
      (version "164")
      (source (origin
		(method url-fetch)
		(uri (string-append "https://api.papermc.io/v2/projects/paper/versions/1.20.1/builds/"
				    version "/downloads/paper-1.20.1-" version ".jar"))
		(sha256
		 (base32
		  "1rdmmi7mkp0ssqpa9pl1rh9n0ypgqcz6yl2n6j62fwzyry6gdy55"))))
      (build-system trivial-build-system)
      (arguments
       `(#:modules ((guix build utils))
	 #:builder
	 (begin
	   (use-modules (guix build utils))
	   (let* ((out (assoc-ref %outputs "out"))
		  (source (assoc-ref %build-inputs "source"))
		  (jar (string-append out "/share/papermc"))
		  (bin (string-append out "/bin"))
		  (jdk (assoc-ref %build-inputs "openjdk")))
	     (mkdir-p jar)
	     (copy-file source (string-append jar "/papermc.jar"))
	     (mkdir-p bin)
	     (with-output-to-file (string-append bin "/paper-1.20.1")
	       (lambda _
		 (display
                  (string-append "#!/bin/sh" "\n"
				 "exec " jdk "/bin/java -jar " jar "/papermc.jar nogui"))))
	     (chmod (string-append bin "/paper-1.20.1") #o755))
	   #t)))
      (propagated-inputs (list `(,openjdk17 "jdk")))
      (home-page "https://papermc.io/")
      (synopsis "A Minecraft game server based on Spigot")
      (description
       "Paper is a Minecraft game server based on Spigot, designed to greatly
improve performance and offer more advanced features and API.")
      ;; Some authors have their work under the MIT license, but PaperMC
      ;; itself is under the GPLv3 license. See the LICENSE.md file in
      ;; their repository for more information.
      (license (list license:gpl3
		     license:expat))))
