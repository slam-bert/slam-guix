;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Slambert <labzero@protonmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (slam-guix packages c)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages python)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) :prefix license:))

(define-public zycore
  (package
   (name "zycore")
   (version "1.4.1")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/zyantific/zycore-c")
		  (commit (string-append "v" version))))
	    (file-name (git-file-name name version))
	    (sha256
             (base32 "0w7ckj6md7bakbnqy0kzqfslvqqz792v8xizqy32jwqyns1596cj"))))
   (build-system cmake-build-system)
   (arguments
    `(#:tests? #f))
   (native-inputs
    (list doxygen))
   (home-page "https://github.com/zyantific/zycore-c")
   (synopsis "Zyan Core Library for C")
   (description "Internal library providing platform independent types,
macros and a fallback for environments without LibC.")
   (license license:expat)))

(define-public zydis
  (package
   (name "zydis")
   (version "4.0.0")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/zyantific/zydis")
		  (commit (string-append "v" version))))
	    (file-name (git-file-name name version))
	    (sha256
             (base32 "0hmlgawb38mzmbin4sm5l69ikk7x72m324lkhmqbkr2sagq3yypy"))))
   (build-system cmake-build-system)
   (arguments
    `(#:tests? #f
      #:configure-flags (list (string-append "-DZYAN_SYSTEM_ZYCORE=ON"))))
   (native-inputs
    (list doxygen
	  python-3))
   (inputs
    (list zycore))
   (home-page "https://zydis.re/")
   (synopsis "Fast and lightweight x86/x86-64 disassembler and code generation library")
   (description "Zydis is a fast and lightweight x86/x86-64 disassembler and
code generation library.")
   (license license:expat)))
